#!/bin/bash
#
# Filename:
# ren_04.sh
#
# Date:
# 2023-11-07
#
# Purpose:
# Renames files with sed and mv.
# One substitution command is prefilled to remove numbers at the end of the filename.
# Befor the files are renamed, the user is shown the result for each file.
# If the result is not as wanted, the sed commands can be edited and tested again before they are applied.
#
# Usage:
# ren_03.sh 'filname' 'search_pattern_1' 'substitution_1' ... 'search_pattern_n' 'substitution_n'
#
# Comments
# IFS=$'\n'
# The first character of IFS is special: It is used to delimit words in the output when using the special $* variable
# "Words of the form $'string' are treated specially.
# The word expands to "string", with backslash-escaped characters replaced as specified by the ANSI C standard."
# \n is the escape sequence for a newline, so IFS ends up being set to a single newline character
# Default of the IFS-Variable: IFS=$' \t\n'

# Copyright (c) 2023
# SPDX-License-Identifier: GPL-3.0-or-later
# https://www.gnu.org/


###### functions ######
testPattern() {
( # Following code runs in a subshell to limit the effect of the IFS modification
IFS=$'\n'
for file_name in $file_name_pattern
do
	echo -e  "$file_name" "\t--> " "$(echo "$file_name" | sed "$search_pattern")"
done
)
}

moveFiles() {
( # Following code runs in a subshell to limit the effect of the IFS modification
IFS=$'\n'
echo "============== move Files ================"
for file_name in $file_name_pattern
do
	#echo -e  "$file_name" "\t--> " "$(echo "$file_name" | sed "$search_pattern")"
	mv -v "$file_name" "$(echo "$file_name" | sed "$search_pattern")"
done
)
}

confirmPattern() {
PS3="Make a selection and confirm your entry. Number: "
	select var in "Change pattern" "Cancel program" "Apply pattern"
	do
		case $REPLY in
			1 )	echo -e "Change replacement pattern:"
				# IFS= --> treat the hole Line as one word to assign to the variable
				# -r -> do not treat the backslash as escape operator
				IFS= read -re -i "${search_pattern}" search_pattern
				testPattern
				confirmPattern
				break;;
			2 )	echo -e "Abort program."
				exit 0
				break;;
			3 )	moveFiles
				break;;
			* )	echo -e "\tInvalid selection." ;;
		esac
	done
}

####### end functions ######

file_name_pattern="$1"
# pre defined substitute command
search_pattern="s/-[[:digit:]]\+\././g"
declare -i i=0

if [ $# -lt 1 ]
then
	echo -e "\tNot enough parameter given ($#)"
	echo -e "\tUsage:"
	echo -e "\tren_03.sh 'filname' 'search_pattern_1' 'substitution_1' ... 'search_pattern_n' 'substitution_n'"
	exit 1
fi

shift
for pattern in "$@"
do
((i++))
	if (( i % 2 ))
	then
		#echo $(( i % 2))
		search_str="$pattern"
	else
		sub_str="$pattern"
		search_pattern="${search_pattern};s/$search_str/$sub_str/g"
		#echo "$search_pattern"
	fi
done

testPattern
confirmPattern

exit 0
